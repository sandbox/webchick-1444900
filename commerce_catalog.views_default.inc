<?php

$view = new view;
$view->name = 'catalog';
$view->description = 'Drupal Commerce product catalog.';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Catalog';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Catalog';
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_catalog',
    'rendered' => 1,
  ),
);
$handler->display->display_options['style_options']['fill_single_line'] = 1;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 1;
/* Relationship: Content: Referenced product */
$handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
$handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['required'] = 1;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['nid']['link_to_node'] = 0;
/* Field: Field: Image */
$handler->display->display_options['fields']['field_image']['id'] = 'field_image';
$handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
$handler->display->display_options['fields']['field_image']['field'] = 'field_image';
$handler->display->display_options['fields']['field_image']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_image']['label'] = '';
$handler->display->display_options['fields']['field_image']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['field_image']['alter']['path'] = 'node/[nid]';
$handler->display->display_options['fields']['field_image']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_image']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_image']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_image']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_image']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_image']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_image']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_image']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_image']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_image']['group_column'] = 'entity id';
$handler->display->display_options['fields']['field_image']['field_api_classes'] = 0;
/* Field: Content: Catalog */
$handler->display->display_options['fields']['field_catalog']['id'] = 'field_catalog';
$handler->display->display_options['fields']['field_catalog']['table'] = 'field_data_field_catalog';
$handler->display->display_options['fields']['field_catalog']['field'] = 'field_catalog';
$handler->display->display_options['fields']['field_catalog']['label'] = '';
$handler->display->display_options['fields']['field_catalog']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_catalog']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_catalog']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_catalog']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_catalog']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_catalog']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_catalog']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_catalog']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_catalog']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_catalog']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_catalog']['group_column'] = 'entity id';
$handler->display->display_options['fields']['field_catalog']['group_rows'] = 1;
$handler->display->display_options['fields']['field_catalog']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_catalog']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_catalog']['delta_first_last'] = 0;
$handler->display->display_options['fields']['field_catalog']['field_api_classes'] = 0;
/* Field: Commerce Product: Price */
$handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
$handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['commerce_price']['label'] = '';
$handler->display->display_options['fields']['commerce_price']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['external'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['commerce_price']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['commerce_price']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['trim'] = 0;
$handler->display->display_options['fields']['commerce_price']['alter']['html'] = 0;
$handler->display->display_options['fields']['commerce_price']['element_type'] = 'strong';
$handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['element_default_classes'] = 1;
$handler->display->display_options['fields']['commerce_price']['hide_empty'] = 0;
$handler->display->display_options['fields']['commerce_price']['empty_zero'] = 0;
$handler->display->display_options['fields']['commerce_price']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_formatted_amount';
$handler->display->display_options['fields']['commerce_price']['settings'] = array(
  'calculation' => '0',
);
$handler->display->display_options['fields']['commerce_price']['group_column'] = 'entity id';
$handler->display->display_options['fields']['commerce_price']['field_api_classes'] = 0;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'product_display' => 'product_display',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'catalog';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Catalog';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;

